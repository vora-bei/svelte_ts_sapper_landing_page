import firestore from '../../firestore';
const blogRefs = firestore.collection('blogs');

export async function get(req, res) {
	res.writeHead(200, {
		'Content-Type': 'application/json'
	});
	const blogs = await blogRefs.get();
	res.end(JSON.stringify(blogs.docs.map(d=>({
		slug: d.id,
		title: d.get('title'),
		description: d.get('description'),
	}))));
}