import firestore from '../../firestore';

export async function get(req, res, next) {
	const { slug } = req.params;
	try {
		const blog = firestore.collection('blogs').doc(slug);
		const result = await blog.get();
		res.writeHead(200, {
			'Content-Type': 'application/json'
		});

		res.end(JSON.stringify({
			title: result.get('title'),
			html: result.get('description'),
		}));
	} catch(e){
		res.writeHead(404, {
			'Content-Type': 'application/json'
		});

		res.end(JSON.stringify({
			message: `Not found`
		}));
	}
}
